package com.edutec.sqlite_example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        SqliteHelper.insert(this,"entrada a la aplicación "+ts);
        String saved =  SqliteHelper.select(this)[0];
        Toast.makeText(this,saved,Toast.LENGTH_LONG).show();
    }
}
