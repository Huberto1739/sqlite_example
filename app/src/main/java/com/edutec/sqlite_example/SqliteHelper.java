package com.edutec.sqlite_example;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Huberto on 24/01/2017.
 */

public class SqliteHelper extends SQLiteOpenHelper {
    //----------------------------------------------------
    static final String HLP_DBNAME = "pdb";
    static final String HLP_TABLENAME = "ptable";
    static final String HLP_TEXT = "text";
    //----------------------------------------------------

    public SqliteHelper(Context context) {
        super(context, HLP_DBNAME, null, 1);
        this.getWritableDatabase().execSQL(
                "create table IF NOT EXISTS "+HLP_TABLENAME+
                        "("
                        +HLP_TEXT+" text)");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static SQLiteDatabase callDB(Context context){
        SqliteHelper admin = new SqliteHelper(context);
        return admin.getWritableDatabase();
    }

    public static void insert(Context context,String text){
        try {
            SQLiteDatabase db = callDB(context);
            ContentValues registro = new ContentValues();
            registro.put(HLP_TEXT, text);
            db.insert(HLP_TABLENAME,null, registro);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String[] select(Context context){
        SQLiteDatabase bd = callDB(context);
        String text = "";
        Cursor cursor = bd.rawQuery(
                "select "+HLP_TEXT+" from "+HLP_TABLENAME, null);

        while(cursor.moveToNext()) {
            try {
                text = text + "\n" + cursor.getString(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        bd.close();
        String ret[] = new String[1];
        ret[0] = text;
        return ret;
    }
    public static void delete(Context context, String text){
        try {
            SQLiteDatabase db = callDB(context);
            db.delete(HLP_TABLENAME, HLP_TEXT + " = '" + text + "'", null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
