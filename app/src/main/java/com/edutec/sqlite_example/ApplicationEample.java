package com.edutec.sqlite_example;

import android.app.Application;
import android.content.Context;

/**
 * Created by Huberto on 27/05/2017.
 */

public class ApplicationEample extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        SqliteHelper sqlh = new SqliteHelper(getBaseContext());

    }
}
